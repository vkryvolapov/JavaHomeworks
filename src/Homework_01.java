import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework_01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Let the game begin! Enter your name:");
        String name = scanner.nextLine();
        int numberFromMachine = new Random().nextInt(101) - 1;
        int numberFromHuman = -1;
        int[] numbersFromHuman = new int[100];
        int countNumbersFromHuman = 0;
        do {
            System.out.println("Guess the number from 0 to 100:");
            while(!scanner.hasNextInt()){
                System.out.println("Please input the number!!");
                scanner.nextLine();
            }
            numberFromHuman = scanner.nextInt();
            if (numberFromHuman == numberFromMachine)
                break;
            System.out.println("Your number is too "
                + (numberFromHuman < numberFromMachine ? "small" : "big"));
            if (countNumbersFromHuman >= numbersFromHuman.length)
            {
                int[] updatedNumbers = new int[(int)(countNumbersFromHuman * 1.5)];
                for (int i = 0; i < numbersFromHuman.length; i++) {
                    updatedNumbers[i] = numbersFromHuman[i];
                }
                numbersFromHuman = updatedNumbers;
            }
            numbersFromHuman[countNumbersFromHuman++]=numberFromHuman;
        } while (true);
        System.out.println("Congratulations, " + name + "!");
        System.out.println("Your previous guesses are:");
        System.out.println(Arrays.toString(Arrays.copyOf(numbersFromHuman, countNumbersFromHuman)));
    }
}
