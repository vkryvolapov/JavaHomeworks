import java.util.Scanner;

public class Homework_03 {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to cinema";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to work; eat, pray, love";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to work; do sports in gym";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to work; visit club";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "do home work; go to courses";

        Scanner scanner = new Scanner(System.in);
        final int exitChoice = -1;
        final int errorChoice = -2;
        while (true) {
            System.out.println("Please, input the day of the week:");
            String input = scanner.nextLine();
            input = input.trim();
            int dayIndex;
            boolean changeRequested = false;
            if (input.toLowerCase().startsWith("change ")) {
                changeRequested = true;
                input = input.substring("change ".length());
            }
            switch (input.toLowerCase()) {
                case "monday":
                    dayIndex = 1;
                    break;
                case "tuesday":
                    dayIndex = 2;
                    break;
                case "wednesday":
                    dayIndex = 3;
                    break;
                case "thursday":
                    dayIndex = 4;
                    break;
                case "friday":
                    dayIndex = 5;
                    break;
                case "saturday":
                    dayIndex = 6;
                    break;
                case "sunday":
                    dayIndex = 0;
                    break;
                case "exit":
                    dayIndex = exitChoice;
                    break;
                default:
                    dayIndex = errorChoice;
                    break;
            }
            if (dayIndex == errorChoice) {
                System.out.println("Sorry, I don't understand you, please try again.");
                continue;
            }
            else if (dayIndex == exitChoice) {
                break;
            }
            if (changeRequested) {
                System.out.println("Please, input new tasks for " + input);
                scedule[dayIndex][1] = scanner.nextLine();
            }
            else
                System.out.println("Your tasks for " + input + ": " + scedule[dayIndex][1]);
        }
    }
}
