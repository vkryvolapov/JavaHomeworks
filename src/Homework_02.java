import java.util.Random;
import java.util.Scanner;

public class Homework_02 {

    public static void printTable(char[][] table, int w, int h) {
        for (int i = 0; i <= w; i++) {
            System.out.print(String.format(" %d |", i));
        }
        System.out.println();
        for (int y = 0; y < h; y++) {
            System.out.print(String.format(" %d |", y + 1));
            for (int x = 0; x < w; x++) {
                System.out.print(String.format(" %c |", table[y][x]));
            }
            System.out.println();
        }
    }

    public static int getUserIntegerChoice(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            if (!scanner.hasNextInt()) {
                System.out.println("Please enter the number!!");
                scanner.next();
                continue;
            }
            int userChoice = scanner.nextInt();
            if (userChoice >= min && userChoice <= max) {
                return userChoice;
            }
            System.out.println("Please enter the number in required boundaries!!");
        }
    }

    public static void main(String[] args) {

        //init field and its params
        final int tableSize = 5;
        final int goalSize = 3;
        final char emptyCellMarker = '-';
        final char shotCellMarker = '*';
        final char goalCellMarker = 'x';
        char[][] field = new char[tableSize][];
        for (int i = 0; i < tableSize; i++) {
            field[i] = new char[tableSize];
            for (int j = 0; j < tableSize; j++) {
                field[i][j] = emptyCellMarker;
            }
        }

        //set goal coords (3x3 size in this case)
        Random randomizer = new Random();
        int xGoal = randomizer.nextInt(tableSize - goalSize);
        int yGoal = randomizer.nextInt(tableSize - goalSize);

        //game main loop
        System.out.println("All set. Get ready to rumble!");
        while (true) {
            //get shot coords
            printTable(field, tableSize, tableSize);
            System.out.println(String.format(
                    "Enter the line number for shooting between 1 and %d",
                    tableSize)
            );
            int yShoot = getUserIntegerChoice(1, tableSize);
            yShoot--;
            System.out.println(String.format(
                    "Enter the column number for shooting between 1 and %d",
                    tableSize)
            );
            int xShoot = getUserIntegerChoice(1, tableSize);
            xShoot--;

            //check if shot is not part of the goal
            if (xShoot < xGoal || xShoot >= xGoal + goalSize
                || yShoot < yGoal || yShoot >= yGoal + goalSize)
            {
                field[yShoot][xShoot] = shotCellMarker;
                System.out.println("You missed!");
                continue;
            }

            //in case goal is shot - check if the WHOLE goal is shot
            System.out.println("You shot goal!");
            field[yShoot][xShoot] = goalCellMarker;
            boolean goalSucceed = true;
            for (int y = 0; y < goalSize; y++) {
                for (int x = 0; x < goalSize; x++) {
                    if (field[y + yGoal][x + xGoal] != goalCellMarker)
                    {
                        y = x = goalSize;//for exit both loops
                        goalSucceed = false;
                    }
                }
            }
            if (goalSucceed)
                break;
        }

        System.out.println("You won!");
        printTable(field, tableSize, tableSize);
    }
}
